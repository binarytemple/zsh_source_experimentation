-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: zsh
Binary: zsh, zsh-doc, zsh-static, zsh-dev, zsh-dbg
Architecture: any all
Version: 4.3.17-1ubuntu1
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Uploaders: Michael Prokop <mika@debian.org>, Axel Beckert <abe@debian.org>, Clint Adams <clint@debian.org>, Frank Terbeck <ft@bewatermyfriend.org>, Richard Hartmann <richih.mailinglist@gmail.com>
Homepage: http://www.zsh.org/
Standards-Version: 3.9.3
Vcs-Browser: http://git.debian.org/?p=collab-maint/zsh.git;a=summary
Vcs-Git: git://git.debian.org/collab-maint/zsh.git
Build-Depends: bsdmainutils, groff-base, libcap-dev [linux-any], libncursesw5-dev, libpcre3-dev, texi2html (>= 1.76-3), texinfo, texlive-latex-base
Package-List: 
 zsh deb shells optional
 zsh-dbg deb debug extra
 zsh-dev deb libdevel optional
 zsh-doc deb doc optional
 zsh-static deb shells optional
Checksums-Sha1: 
 d9c9b3589046b9a1d445b25eb1dda20c62bad6a3 2978903 zsh_4.3.17.orig.tar.bz2
 d648d6ecc82b81f2f7ae0d5257f9b53074d6c675 149076 zsh_4.3.17-1ubuntu1.debian.tar.gz
Checksums-Sha256: 
 054e0452afd9c742c9f1489465175e1d4d7db50d88b602d132551d850cf7a704 2978903 zsh_4.3.17.orig.tar.bz2
 e2e066322a0ff7e653c08b0595ee3d339b818c805002b2257125eeaa8c99b655 149076 zsh_4.3.17-1ubuntu1.debian.tar.gz
Files: 
 8258967060b2654f30001a011946ac6a 2978903 zsh_4.3.17.orig.tar.bz2
 eccf2a908d3917dc166d3cdbce98a5f7 149076 zsh_4.3.17-1ubuntu1.debian.tar.gz
Original-Maintainer: Debian Zsh Maintainers <pkg-zsh-devel@lists.alioth.debian.org>

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iEYEARECAAYFAk91S6cACgkQRjrlnQWd1etu0gCZAedndF3pcCE/pdwf16GEvRiz
/UIAnA/kAE/TLpfdPBKCU5p5EgtmImzt
=NwQo
-----END PGP SIGNATURE-----
